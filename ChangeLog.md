## 3.17

### hys
1. adding choosing server algorithm (may simply sort using some measure)
2. change the structure of totalRequestInfos


### cs
1. test

## 3.18

### hys
1. add bugServer(), the initial idea is to buy server that has the same core and memory ratio with daily required VM. then combine the factor of core/memory ratio, hardware cost and power cost to choose a type of server and the number of such type of server satisfy the daily requirement
2. problem
   1. cannot use the core/memory ratio of daily requirement in comparison function compareServerMeasure(), so I use 1:1, since the average core/memory ratio of daily requirement is 4408/5080
   2. the server that satisfy the choosing algorithm has small core and memory resource, cannot satisfy all VM (even the first requested VM)
3. TODO: server & vm matching, server extension