#include "function.h"

#ifndef INCLUDE_TIME
#include <time.h>
#define INCLUDE_TIME
#endif // !INCLUDE_TIME

using namespace std;

#define TEST

int main() {
    clock_t start, finish;
    start = clock();
    CenterProcess centerProcess;
    finish = clock();
    cout << "time: " << finish - start << endl;
    return 0;
}